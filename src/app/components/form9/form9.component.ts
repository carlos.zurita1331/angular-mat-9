import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form9',
  templateUrl: './form9.component.html',
  styleUrls: ['./form9.component.css']
})
export class fom9Component implements OnInit {

  form!: FormGroup;
  foco: boolean = false;
  formB!: FormGroup;
  lista!: string;
  insert: string[] = [];
  ListaFinal!: string;
  ayuda:string = '';

  constructor(
    private fb: FormBuilder

  ) { 
    this.crearFormulario();
  }

  ngOnInit(): void {
  }


  crearFormulario(): void {
    this.form = this.fb.group(
      {
        id: [''],
        utiles: this.fb.array([[this.formB = this.fb.group({
          condicion: ['',],
          name: ['', [Validators.pattern(/^[0-9a-zA-zñÑ\s]+$/)]],
        })
        ]]),
      }
    )
  }



  get nombreInvalido() {

    return (
      this.formB.get('name')?.invalid && this.formB.get('name')?.touched
    );
  }

  get MatEscolar() {
    return this.form.get('utiles') as FormArray;
  }

  agregarMat(): void {
    this.MatEscolar.push(this.fb.control(''))
    this.formB.patchValue({
      name: ''
    })
    
  }

  eliminarMat(id: number): void {
    this.MatEscolar.removeAt(id)

  }

  eliminarUtiles(): void {

    this.form = this.fb.group(
      {
        utiles: this.fb.array([])
      }
    )
  }

  cleanBox(): void { 

   this.formB.reset({
     name: ''
   })
   this.insert = [];
   this.ListaFinal = this.insert.toString();
  }


 saveBox(): void {

    if (this.formB.value.condicion === true) {

      this.lista = this.formB.value.name;

      this.insert.push(this.lista)
      

      this.ListaFinal = this.insert.join('\n')


      this.ListaFinal = this.ListaFinal.split("\n").join("<br>")
      

      this.formB.patchValue({
        condicion: ''
      })
    }
  }
}
