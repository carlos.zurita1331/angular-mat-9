import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ɵInternalFormsSharedModule } from '@angular/forms';
import { fom9Component } from './components/form9/form9.component';
import { SharedModule } from './components/shared/shared.module';

import { MatFormFieldModule } from '@angular/material/form-field';


@NgModule({
  declarations: [
    AppComponent,
    fom9Component
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ɵInternalFormsSharedModule,
    SharedModule,
    MatFormFieldModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
